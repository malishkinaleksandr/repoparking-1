﻿using Parking.Veihcle;

namespace Parking.ParkingSpace
{
    public abstract class BaseParkingSpase
    {
        public bool IsFree { get; protected set; }
        public int ID { get; protected set; }
        public BaseVehicle Vehicle { get; protected set; }

        public virtual bool Add(BaseVehicle vehicle)
        {
            if (IsFree)
            {
                Vehicle = vehicle;
                IsFree = false;
                return true;
            }
            return false;
        }

        public virtual bool Subtract()
        {
            if (IsFree)
            {
                return false;
            }

            Vehicle = null;
            return IsFree = true;
        }

        public virtual void Replace(BaseVehicle vehicle)
        {
            Vehicle = vehicle;
        }
    }
}
